'use strict'

const {Gio, Gtk} = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const gettextDomain = Me.metadata['gettext-domain'];
const Gettext = imports.gettext;
const Domain = Gettext.domain(gettextDomain);
const _ = Domain.gettext;
const ngettext = Domain.ngettext;

const Config = imports.misc.config;
const shellVersion = parseFloat(Config.PACKAGE_VERSION);

const Battery = Me.imports.libs.Battery;
const UIFolderPath = Me.dir.get_child('ui').get_path();


var Prefs = class {

    /**
     * Class contructor
     */
    constructor() {
        this._settings = ExtensionUtils.getSettings();
        this._builder = new Gtk.Builder();

        this._bat0 = new Battery.Threshold(0);
        this._bat1 = new Battery.Threshold(1);

        this._builder.set_translation_domain(gettextDomain);
    }

    /**
     * Get main prefs widget.
     *
     * @param {string} UIFolderPath folder path to ui folder
     * @param {string} gettextDomain gettext domain
     *
     * @returns {Object}
     */
    getMainPrefs() {
        this._builder.add_from_file(UIFolderPath + '/prefs.ui');

        const mainPrefs = this._builder.get_object('main_prefs');

        this._setTitles();

        const headerBar = this._builder.get_object('header_bar');
        
        mainPrefs.connect('realize', () => {
            const window = mainPrefs.get_root();
            window.set_titlebar(headerBar);
            this._connectSignals(window);
        });

        return mainPrefs;
    }

    /**
     * Fill prefs window.
     *
     * @param {AdwWindow} window 
     * @param {string} UIFolderPath folder path to ui folder
     * @param {string} gettextDomain gettext domain
     *
     * @returns {void}
     */
    fillPrefsWindow(window) {
        this._builder.add_from_file(UIFolderPath + '/adw/prefs.ui');

        const mainPrefs = this._builder.get_object('main_prefs');

        this._connectSignals(window);
        this._setTitles();

        window.add(mainPrefs);
        //window.search_enabled = true;
    }

    /**
     * Connect widges signals.
     * 
     * @param {Gtk.Window} window
     */
    _connectSignals(window) {
        this._bindDropdown('indicator_mode', 'indicator-mode');
        this._bindDropdown('icons_theme', 'icons-theme');
        this._bindSwitch('show_values', 'show-current-values');
        this._bindSwitch('show_tooltips', 'show-tooltips');
        this._bindSwitch('show_notifications', 'show-notifications');

        this._bindSpinbutton('start_bat0', 'start-bat0');
        this._bindSpinbutton('stop_bat0', 'stop-bat0');
        this._bindSpinbutton('start_bat1', 'start-bat1');
        this._bindSpinbutton('stop_bat1', 'stop-bat1');

        // Rules
        const startBat0 = this._builder.get_object('start_bat0');
        const stopBat0 = this._builder.get_object('stop_bat0');
        const startBa1 = this._builder.get_object('start_bat1');
        const stopBat1 = this._builder.get_object('stop_bat1');

        this._settings.connect('changed::start-bat0', () => {
            if (startBat0.value >= stopBat0.value) {
                stopBat0.value = startBat0.value + 5;
            }
        });
        this._settings.connect('changed::stop-bat0', () => {
            if (startBat0.value >= stopBat0.value) {
                startBat0.value = stopBat0.value - 5;
            }
        });
        this._settings.connect('changed::start-bat1', () => {
            if (startBa1.value >= stopBat1.value) {
                stopBat1.value = startBa1.value + 5;
            }
        });
        this._settings.connect('changed::stop-bat1', () => {
            if (startBa1.value >= stopBat1.value) {
                startBa1.value = stopBat1.value - 5;
            }
        });
    }

    /**
     * Set groups titles.
     */
    _setTitles() {
        const bat0Title = this._bat0.isAvailable ? _('Main battery (BAT0)') : _('Main battery (Not available)');
        const bat1Title = this._bat1.isAvailable ? _('Secondary battery (BAT1)') : _('Secondary battery (Not available)');
        if (shellVersion < 42) {
            this._builder.get_object('bat0_title').label = bat0Title;
            this._builder.get_object('bat1_title').label = bat1Title;
        } else {
            this._builder.get_object('bat0_group').title = bat0Title;
            this._builder.get_object('bat1_group').title = bat1Title;
        }
    }

    /**
     * Bind GtkSwitch.
     * 
     * @param {string} objectName Object id
     * @param {string} settingKey Settings key
     */
    _bindSwitch(objectName, settingKey) {
        const object = this._builder.get_object(objectName);
        this._settings.bind(settingKey, object, 'state', Gio.SettingsBindFlags.DEFAULT);
    }

    /**
     * Bind GtkDropDown.
     * 
     * @param {string} objectName Object id
     * @param {string} settingKey Settings key
     */
    _bindDropdown(objectName, settingKey) {
        const object = this._builder.get_object(objectName);
        object.selected = this._settings.get_enum(settingKey);
        object.connect('notify::selected-item', () => {
            this._settings.set_enum(settingKey, object.selected);
        });
    }

    /**
     * Bind GtkSpinbutton.
     * 
     * @param {string} objectName Object id
     * @param {string} settingKey Settings key
     */
    _bindSpinbutton(objectName, settingKey) {
        const object = this._builder.get_object(objectName);
        this._settings.bind(settingKey, object, 'value', Gio.SettingsBindFlags.DEFAULT);
    }
}
