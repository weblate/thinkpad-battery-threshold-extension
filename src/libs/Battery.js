'use strict';

const Gettext = imports.gettext;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Domain = Gettext.domain(Me.metadata.uuid);
const _ = Domain.gettext;

const {GLib, Gio} = imports.gi;
const ByteArray = imports.byteArray;

// Base path
const BASE_PATH = '/sys/class/power_supply/';

// Threshold paths
const START_PATHS = [
    '/charge_control_start_threshold',
    '/charge_start_threshold'
];
const STOP_PATHS = [
    '/charge_control_end_threshold',
    '/charge_stop_threshold'
];

var Threshold = class Threshold {

    /**
     * 
     * @param {number} bat Battery number
     */
    constructor(bat) {
        this.settings = ExtensionUtils.getSettings();

        this._bat = bat;

        const batPath = BASE_PATH + 'BAT' + this._bat;

        // Start paths
        this._startFile = null;
        for (const path of START_PATHS) {
            this._startFile = Gio.File.new_for_path(batPath + path);
            if (this.startAvailable) {
                if (this._canWrite(this._startFile)) break;
            }
        }

        // Stop paths
        this._stopFile = null;
        for (const path of STOP_PATHS) {
            this._stopFile = Gio.File.new_for_path(batPath + path);
            if (this.stopAvailable) {
                if (this._canWrite(this._startFile)) break;
            }
        }
    }

    /**
     * Return battery number.
     * 
     * @returns {number} Battery number (0 or 1)
     */
    get batNumber() {
        return this._bat;
    }

    /**
     * Return start file.
     * 
     * @returns {(Gio.File|null)} Gio.File or null if not available
     */
    get startFile() {
        return this._startFile;
    }

    /**
     * Return stop file.
     * 
     * @returns {(Gio.File|null)} Gio.File or null if not available
     */
    get stopFile() {
        return this._stopFile;
    }

    /**
     * Enable values monitor.
     * 
     * @param {Function} callback Callback function
     */
    enableMonitors(callback) {
        try {
            this._startMonitor = this.startFile.monitor_file(Gio.FileMonitorFlags.NONE, null);
            this._startMonitor.connect("changed", (_, file, otherFile, eventType) => {
                switch (eventType) {
                    case Gio.FileMonitorEvent.CHANGES_DONE_HINT:
                    case Gio.FileMonitorEvent.CREATED:
                    case Gio.FileMonitorEvent.DELETED:
                        callback();
                        break;
                    default:
                        break;
                }
            });
        } catch (e) {
            logError(e, 'startMonitors')
        }
        try {
            this._stopMonitor = this.stopFile.monitor_file(Gio.FileMonitorFlags.NONE, null);
            this._stopMonitor.connect("changed", (_, file, otherFile, eventType) => {
                switch (eventType) {
                    case Gio.FileMonitorEvent.CHANGES_DONE_HINT:
                    case Gio.FileMonitorEvent.CREATED:
                    case Gio.FileMonitorEvent.DELETED:
                        callback();
                        break;
                    default:
                        break;
                }
            });        
        } catch (e) {
            logError(e, 'startMonitors')
        }
    }

    /**
     * Stop changes monitors
     */
    disableMonitors() {
        if (this._startMonitor) {
            this._startMonitor.cancel();
            this._startMonitor = null;
        }
        if (this._stopMonitor) {
            this._stopMonitor.cancel();
            this._stopMonitor = null;
        }
    }

    /**
     * Get if start threshold is available.
     * 
     * @returns {boolean} True if available, false otherwise
     */
    get startAvailable() {
        try {
            const info = this.startFile.query_info('access::*', Gio.FileQueryInfoFlags.NONE, null);
            return info.get_attribute_boolean('access::can-read');
        } catch (_) {
            return false;
        }
    }

    /**
     * Get if stop threshold is available.
     * 
     * @returns {boolean} True if available, false otherwise
     */
    get stopAvailable() {
        try {
            const info = this.stopFile.query_info('access::*', Gio.FileQueryInfoFlags.NONE, null);
            return info.get_attribute_boolean('access::can-read');
        } catch (_) {
            return false;
        }
    }

    /**
     * Get if any of thresholds is available.
     * 
     * @returns {boolean} True if any of thresholds is availables, false otherwise
     */
    get isAvailable() {
        return this.startAvailable || this.stopAvailable;
    }

    /**
     * Check if I have write permissions.
     * 
     * @param {Gio.File} file 
     * @returns {boolean} True if I have write permissions, false otherwise 
     */
    _canWrite(file) {
        try {
            const info = file.query_info('access::*', Gio.FileQueryInfoFlags.NONE, null);
            return info.get_attribute_boolean('access::can-write');
        } catch (_) {
            return false;
        }
    }

    /**
     * Check if root permissions are needed to modify the values.
     * 
     * @returns {boolean} True if we need root permissions, false otherwise
     */
    get needPermissions() {
        if (this.startAvailable) {
            if (!this._canWrite(this.startFile)) {
                return true;
            }
        }
        if (this.stopAvailable) {
            if (!this._canWrite(this.stopFile)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get current start threshold value.
     * 
     * @returns {number} Return current start threshold value or 0 if not available
     */
    get currentStartValue() {
        try {
            const [, value,] = this.startFile.load_contents(null);
            return parseInt(ByteArray.toString(value));
        } catch (e) {
            logError(e, 'currentStartValue');
            return 0;
        } 
    }

    /**
     * Get current stop threshold value.
     * 
     * @returns {number} Return current stop threshold value or 100 if not available
     */
     get currentStopValue() {
        try {
            const [, value,] = this.stopFile.load_contents(null);
            return parseInt(ByteArray.toString(value));
        } catch (e) {
            logError(e, 'currentStopValue');
            return 100;
        } 
    }

    /**
     * Return current start and stop threshold values.
     * 
     * @returns {[number, number]}
     */
    get currentThreshold() {
        return [this.currentStartValue, this.currentStopValue];
    }

    /**
     * Return true if threshold is enabled.
     * 
     * @returns {boolean} True if thresholds is enabled, false otherwise
     */
    get isEnabled() {
        if (!this.isAvailable) return false;
        const current = this.currentThreshold;
        return current[0] !== 0 || current[1] !== 100;
    }

    /**
     * Toggle the battery charge thresholds.
     * 
     * @returns {(Promise<[number, number]>|Promise<Error|null>)} A promise on the execution of the command to modify the values of the thresholds      */
    toggleThreshold() {
        const start = this.isEnabled ? 0 : this.settings.get_int('start-bat' + this._bat);
        const stop = this.isEnabled ? 100 : this.settings.get_int('stop-bat' + this._bat);
        return this.setThreshold(start, stop);
    }

    /**
     * Enable battery thresholds.
     * 
     * @returns {(Promise<[number, number]>|Promise<Error|null>)} A promise on the execution of the command to modify the values of the thresholds      */
    enableThreshold() {
        const start = this.settings.get_int('start-bat' + this._bat);
        const stop = this.settings.get_int('stop-bat' + this._bat);
        return this.setThreshold(start, stop);
    }

    /**
     * Disable battery thresholds.
     * 
     * @returns {(Promise<[number, number]>|Promise<Error|null>)} A promise on the execution of the command to modify the values of the thresholds      */
    disableThreshold() {
        return this.setThreshold(0, 100);
    }

    /**
     * Set the battery charge thresholds.
     * 
     * @param {number} start Start value
     * @param {number} stop Stop value
     * @returns {(Promise<[number, number]>|Promise<Error|null>)} A promise on the execution of the command to modify the values of the thresholds      
     * */
    setThreshold(start, stop) {       
        return new Promise((resolve, reject) => {

            // Check integers
            if (!Number.isInteger(start) || !Number.isInteger(stop)) {
                // TRANSLATORS: The first %s is the start value and the second %s is the stop value
                const e = new Error(_('Invalid threshold values (%s/%s)').format(start, stop));
                logError(e);
                reject(e);
                return;
            }
            // Check that the start is less than the stop, that the start is greater than or equal to zero, 
            // and that the stop is less than or equal to 100
            if (start >= stop || start < 0 || stop > 100) {
                // TRANSLATORS: The first %s is the start value and the second %s is the stop value
                const e = new Error(_('Invalid threshold values (%s/%s)').format(start.toString(), stop.toString()));
                logError(e);
                reject(e);
                return;
            }

            // Reimplementation inspired by: https://github.com/linrunner/TLP/blob/main/bat.d/05-thinkpad

            // Files paths and commands
            const startPath = this.startFile.get_path();
            const stopPath = this.stopFile.get_path();
            const setStart = `echo ${start.toString()} > ${startPath}`;
            const setStop = `echo ${stop.toString()} > ${stopPath}`;
            
            let [oldStart, oldStop] = this.currentThreshold;

            if ((oldStart == start || !startPath) && (oldStop == stop) || !stopPath) {
                // Same thresholds
                reject(null);
                return;
            }

            if (oldStart >= oldStop) {
                // Invalid threshold reading, happens on ThinkPad E/L series
                oldStart = null;
                oldStop = null;
            }

            let command = '';

            if (startPath && stopPath) {
                if (oldStart == start) { // Same start, apply only stop
                    command = setStop;
                } else if (oldStop == stop) { // Same stop, apply only start
                    command = setStart;
                } else {
                    // Determine sequence
                    let startStopSequence = true;
                    if (oldStop != null && start > oldStop) {
                        startStopSequence = false;
                    }
                    if (startStopSequence) {
                        command = setStart + ' && ' + setStop;
                    } else {
                        command = setStop + ' && ' + setStart;
                    }
                }
            } else if (startPath) { // Only start available
                command = setStart;
            } else if (stopPath) { // Only stop available
                command = setStop;
            } else {
                const e = new Error(_('Thresholds not available'));
                logError(e);
                reject(e);
                return;
            }

            const argv = ['sh', '-c', command];

            if (this.needPermissions) {
                argv.unshift('pkexec');
            }

            try {
                let [, pid] = GLib.spawn_async(null, argv, null, GLib.SpawnFlags.SEARCH_PATH | GLib.SpawnFlags.DO_NOT_REAP_CHILD, null);

                GLib.child_watch_add(GLib.PRIORITY_DEFAULT_IDLE, pid, (pid, status) => {
                    try {
                        GLib.spawn_check_exit_status(status);
                        resolve(this.currentThreshold);
                    } catch(e) {
                        if (e.code == 126) { // Cancelled
                            reject(null);
                        } else {
                            logError(e);
                            reject(e);
                        }
                    }
                    GLib.spawn_close_pid(pid);
                });
            } catch (e) {
                logError(e);
                reject(e);
            }
        });
    }

    /**
     * Release resources.
     */
    destroy() {
        this.disableMonitors();
        this._startFile.unref();
        this._stopFile.unref();
        delete this._startFile;
        delete this._stopFile;
    }

};
