'use strict';

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Prefs = Me.imports.libs.Prefs;


/**
 * prefs initiation
 *
 * @returns {void}
 */
function init() {
    ExtensionUtils.initTranslations();
}

/**
 * fill prefs window
 *
 * @returns {Adw.PreferencesWindow}
 */
function fillPreferencesWindow(window) {
    const prefs = new Prefs.Prefs();
    prefs.fillPrefsWindow(window);
}
 

/**
 * prefs widget
 *
 * @returns {Gtk.Widget}
 */
function buildPrefsWidget() {
    const prefs = new Prefs.Prefs();
    return prefs.getMainPrefs();
}
