/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.    If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

/* CHANGELOG: https://gitlab.com/marcosdalvarez/thinkpad-battery-threshold-extension/-/blob/main/CHANGELOG.md */

'use strict';

const {GLib, Gio, GObject, St, Clutter} = imports.gi;

const Config = imports.misc.config;
const [major] = Config.PACKAGE_VERSION.split('.');
const shellVersion = Number.parseInt(major);

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const AggregateMenu = Main.panel.statusArea.aggregateMenu;

const gettextDomain = Me.metadata['gettext-domain'];
const Gettext = imports.gettext;
const Domain = Gettext.domain(gettextDomain);
const _ = Domain.gettext;
const ngettext = Domain.ngettext;

const Battery = Me.imports.libs.Battery;
const Utils = Me.imports.libs.Utils;

let IconsFolderPath = Me.dir.get_child('icons').get_path();

if (shellVersion == 42) {
    IconsFolderPath = Me.dir.get_child('icons/42').get_path();
}

const ThinkpadThresholdIndicator = GObject.registerClass({
    GTypeName: 'ThinkpadThresholdIndicator',
}, class ThinkpadThresholdIndicator extends PanelMenu.SystemIndicator {
    _init() {
        super._init();
        this.settings = ExtensionUtils.getSettings();

        this._bat0 = new Battery.Threshold(0);
        this._bat1 = new Battery.Threshold(1);

        // Indicator
        this._indicator = this._addIndicator();
        this._indicator.gicon = Gio.icon_new_for_string(IconsFolderPath + "/threshold-app-symbolic.svg");
        this._indicator.visible = true;
        AggregateMenu._indicators.insert_child_at_index(this, 0);
        AggregateMenu._thinkpadThreshold = this;
        
        // Main menu
        this._item = new PopupMenu.PopupSubMenuMenuItem(_('Battery Threshold'), true);
        this._item.icon.gicon = this._indicator.gicon;
        this._item.label.clutter_text.x_expand = true;
        this.menu.addMenuItem(this._item);

        // Bateries indicators
        this._bat0Status = new Utils.BatStatus();
        this._bat1Status = new Utils.BatStatus();
        this._item.actor.insert_child_below(this._bat0Status, this._item._triangleBin);
        this._item.actor.insert_child_below(this._bat1Status, this._item._triangleBin);

        // Unavailable
        this._unavailableLabel = new PopupMenu.PopupMenuItem(_('Thresholds not available'));
        this._unavailableLabel.sensitive = false;
        this._item.menu.addMenuItem(this._unavailableLabel);

        // Main bat toggle menu item
        this._bat0Toggle = new PopupMenu.PopupMenuItem(_('Enable thresholds (BAT0)'));
        this._bat0Toggle.connect('activate', () => {
            if (this._bat0.isAvailable) {
                this._bat0.toggleThreshold().then(
                    this._mainSuccess.bind(this), 
                    this._mainFailure.bind(this)
                );
            }
        });
        this._item.menu.addMenuItem(this._bat0Toggle);

        this._applyBat0 = new PopupMenu.PopupMenuItem(_('Apply (BAT0)'));
        this._applyBat0.connect('activate', () => {
            if (this._bat0.isAvailable) {
                this._bat0.enableThreshold().then(
                    this._mainSuccess.bind(this), 
                    this._mainFailure.bind(this)
                );
            }
        });
        this._item.menu.addMenuItem(this._applyBat0);

        // Dock bat toggle menu item
        this._bat1Toggle = new PopupMenu.PopupMenuItem(_('Enable thresholds (BAT1)'));
        this._bat1Toggle.connect('activate', () => {
            if (this._bat1.isAvailable) {
                this._bat1.toggleThreshold().then(
                    this._mainSuccess.bind(this), 
                    this._mainFailure.bind(this)
                );
            }
        });
        this._item.menu.addMenuItem(this._bat1Toggle);

        this._applyBat1 = new PopupMenu.PopupMenuItem(_('Apply (BAT1)'));
        this._applyBat1.connect('activate', () => {
            if (this._bat1.isAvailable) {
                this._bat1.enableThreshold().then(
                    this._mainSuccess.bind(this), 
                    this._mainFailure.bind(this)
                );
            }
        });
        this._item.menu.addMenuItem(this._applyBat1);

        // Separator
        this._item.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        // Settings menu item
        this._config = new PopupMenu.PopupMenuItem(_('Thresholds settings'));
        this._config.connect('activate', () => {
            ExtensionUtils.openPrefs();
        });
        this._item.menu.addMenuItem(this._config);

        // Search power menu and add below it
        const menuItems = AggregateMenu.menu._getMenuItems();
        const powerMenuIndex = AggregateMenu._power ? menuItems.indexOf(AggregateMenu._power.menu) : -1;
        const menuIndex = powerMenuIndex > -1 ? powerMenuIndex : 4;
        AggregateMenu.menu.addMenuItem(this.menu, menuIndex + 1);
        
        this._updateGUI();

        this._bat0.enableMonitors(this._updateGUI.bind(this));
        this._bat1.enableMonitors(this._updateGUI.bind(this));

        this.settings.connect('changed', this._updateGUI.bind(this));
    }

    /**
     * Return full path icon.
     * 
     * @param {string} name Name of icon
     * @returns {string} Icon full path
     */
    _getIconPath(name) {
        const iconsTheme = this.settings.get_enum('icons-theme');
        return IconsFolderPath + '/' + name + (iconsTheme == 0 ? '' : '-symbolic') + '.svg';;
    }

    /**
     * Success callback.
     * 
     * @param {[number, number]} result Threshold values applied
     * @param {boolean} isDock True if it is the secondary battery
     */
    _thresholdSuccessCallback(result, isDock) {
        if (!this._showNotifications) return
        let message = '';
        let iconPath = '';
        if (result[0] === 0 && result[1] === 100) {
            iconPath = this._getIconPath('threshold-inactive');
            if (isDock) {
                message = _('Secondary battery thresholds disabled')
            } else {
                message = _('Main battery thresholds disabled')
            }
        } else {
            iconPath = this._getIconPath('threshold-active');
            if (isDock) {
                // TRANSLATORS: The first %d is the start value and the second %d is the stop value. The string %% is the percent symbol (may need to be escaped depending on the language)
                message = _('Secondary battery charge thresholds enabled at %d/%d %%').format(result[0], result[1]);
            } else {
                // TRANSLATORS: The first %d is the start value and the second %d is the stop value. The string %% is the percent symbol (may need to be escaped depending on the language)
                message = _('Main battery charge thresholds enabled at %d/%d %%').format(result[0], result[1]);
            }
        }
        const icon = Gio.icon_new_for_string(iconPath);
        Utils.notify(_('Battery Threshold'), message, icon);
    }

    _mainSuccess(result) {
        this._thresholdSuccessCallback(result, false);
    }

    _secondarySuccess(result) {
        this._thresholdSuccessCallback(result, true);
    }

    /**
     * Failure callback.
     * 
     * @param {Error} error Error applying thresholds
     * @param {boolean} isDock True if it is the secondary battery
     */
    _thresholdFailureCallback(error, isDock) {
        if (!this._showNotifications) return
        if (error !== null) {
            const icon = Gio.icon_new_for_string(this._getIconPath('threshold-error'));
            let message = error.message;
            if (isDock) {
                message = _('Dock battery error: ') + message;
            } else {
                message = _('Main battery error: ') + message;
            }
            Utils.notify(_('Battery Threshold'), message, icon);
        }
        this._updateGUI();
    }

    _mainFailure(error) {
        this._thresholdFailureCallback(error, false);
    }

    _secondaryFailure(error) {
        this._thresholdFailureCallback(error, true);
    }
    
    /**
     * Update GUI elements.
     */
    _updateGUI() {
        const indicatorMode = this.settings.get_enum('indicator-mode');
        const showValues = this.settings.get_boolean('show-current-values');
        const showTooltips = this.settings.get_boolean('show-tooltips');
        this._showNotifications = this.settings.get_boolean('show-notifications');
        const startBat0 = this.settings.get_int('start-bat0');
        const stopBat0 = this.settings.get_int('stop-bat0');
        const startBat1 = this.settings.get_int('start-bat1');
        const stopBat1 = this.settings.get_int('stop-bat1');

        this._bat0Status.label.visible = showValues;
        this._bat1Status.label.visible = showValues;

        let indicatorIconPath = this._getIconPath('threshold-unknown');
        if (this._bat0.isAvailable || this._bat1.isAvailable) {
            // Main
            this._bat0Toggle.visible = this._bat0.isAvailable;
            this._bat0Toggle.label.text = this._bat0.isEnabled ? _('Disable thresholds (BAT0)') : _('Enable thresholds (BAT0)');

            if (this._bat0.isAvailable) {
                if (this._bat0.isEnabled) {
                    this._bat0Status.icon.gicon = Gio.icon_new_for_string(this._getIconPath('threshold-active'));
                    if (startBat0 != this._bat0.currentStartValue || 
                        stopBat0 != this._bat0.currentStopValue) {
                        this._applyBat0.visible = true;
                    } else {
                        this._applyBat0.visible = false;
                    }
                } else {
                    this._applyBat0.visible = false;
                    this._bat0Status.icon.gicon = Gio.icon_new_for_string(this._getIconPath('threshold-inactive'));
                }
                // TRANSLATORS: The first %d is the start value and the second %d is the stop value. The string %% is the percent symbol (may need to be escaped depending on the language)
                this._bat0Status.label.text = _('%d/%d %%').format(this._bat0.currentStartValue, this._bat0.currentStopValue);
                // TRANSLATORS: The first %d is the start value and the second %d is the stop value. The string %% is the percent symbol (may need to be escaped depending on the language)
                this._bat0Status.tooltip.text = showTooltips ? _('Main battery (%d/%d %%)').format(this._bat0.currentStartValue, this._bat0.currentStopValue) : null;    
            } else {
                this._applyBat0.visible = false;
                this._bat0Status.icon.gicon = Gio.icon_new_for_string(this._getIconPath('threshold-unknown'));
            }
            this._bat0Status.visible = this._bat0.isAvailable;

            // Dock
            this._bat1Toggle.visible = this._bat1.isAvailable;
            this._bat1Toggle.label.text = this._bat1.isEnabled ? _('Disable thresholds (BAT1)') : _('Enable thresholds (BAT1)');

            if (this._bat1.isAvailable) {
                if (this._bat1.isEnabled) {
                    this._bat1Status.icon.gicon = Gio.icon_new_for_string(this._getIconPath('threshold-active'));
                    if (startBat1 != this._bat1.currentStartValue || 
                        stopBat1 != this._bat1.currentStopValue) {
                        this._applyBat1.visible = true;
                    } else {
                        this._applyBat1.visible = false;
                    }
                } else {
                    this._applyBat1.visible = false;
                    this._bat1Status.icon.gicon = Gio.icon_new_for_string(this._getIconPath('threshold-inactive'));
                }
                // TRANSLATORS: The first %d is the start value and the second %d is the stop value. The string %% is the percent symbol (may need to be escaped depending on the language)
                this._bat1Status.label.text = _('%d/%d %%').format(this._bat1.currentStartValue, this._bat1.currentStopValue);
                // TRANSLATORS: The first %d is the start value and the second %d is the stop value. The string %% is the percent symbol (may need to be escaped depending on the language)
                this._bat1Status.tooltip.text = showTooltips ? _('Dock battery (%d/%d %%)').format(this._bat1.currentStartValue, this._bat1.currentStopValue) : null;     
            } else {
                this._applyBat1.visible = false;
                this._bat1Status.icon.gicon = Gio.icon_new_for_string(this._getIconPath('threshold-unknown'));
            }
            this._bat1Status.visible = this._bat1.isAvailable;

            // Indicator
            if (this._bat0.isEnabled || this._bat1.isEnabled) {
                indicatorIconPath = this._getIconPath('threshold-active');
            } else if (!(this._bat0.isEnabled && this._bat1.isEnabled)) {
                indicatorIconPath = this._getIconPath('threshold-inactive');
            }

            this._unavailableLabel.visible = false;
        } else {
            this._unavailableLabel.visible = true;
            this._bat0Toggle.visible = false;
            this._bat1Toggle.visible = false;
            this._bat0Status.visible = false;
            this._bat1Status.visible = false;
            this._applyBat0.visible = false;
            this._applyBat1.visible = false;
        }

        switch (indicatorMode) {
            case 1:
                this._indicator.visible = true;
                break;
            case 2:
                this._indicator.visible = false;
                break;
            default:
                this._indicator.visible = this._bat0.isEnabled || this._bat1.isEnabled;
                break;
        }
        
        this._indicator.gicon = Gio.icon_new_for_string(indicatorIconPath);
    }

    destroy() {
        this._bat0.destroy();
        this._bat1.destroy();

        this._item.destroy();
        this.menu.destroy();
        delete AggregateMenu._mainBat;
        
        super.destroy();
    }
});

var thresholdIndicator = null;

function init() {
    ExtensionUtils.initTranslations();
}

function enable() {
    thresholdIndicator = new ThinkpadThresholdIndicator();
}

function disable() {
    thresholdIndicator.destroy();
    thresholdIndicator = null;
}
